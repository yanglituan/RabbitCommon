<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>CDlgAbout</name>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="20"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="58"/>
        <source>Informatioin</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="124"/>
        <source>Home page: https://github.com/KangLin/Tasks.git</source>
        <translation>主页： https://github.com/KangLin/Tasks.git</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="83"/>
        <source>Version: 1.0.0.0</source>
        <translation>版本： 1.0.0.0</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="105"/>
        <source>Build Date:</source>
        <translation>编译日期：</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="219"/>
        <source>Donation</source>
        <translation>捐赠</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="243"/>
        <source>License</source>
        <translation>许可协议</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="257"/>
        <source>Change log</source>
        <translation>修改日志</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="271"/>
        <source>Thanks</source>
        <translation>感谢</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="468"/>
        <source> Copyright (C) 2018 KangLin Studio</source>
        <translation>版权(C) 2018 康林工作室</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="475"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="26"/>
        <source>Author:KangLin</source>
        <translation>作者： 康林</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="410"/>
        <source>Tasks</source>
        <translation>任务</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="33"/>
        <source> Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="33"/>
        <source> Arch:</source>
        <translation> 架构：</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="34"/>
        <source>Build date:%1 %2</source>
        <translation>编译日期：%1 %2</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="35"/>
        <source>Author: KangLin
Email:kl222@126.com</source>
        <translation>作者： 康林
邮件:kl222@126.com</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="38"/>
        <source>Home page:</source>
        <translation>主页：</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="40"/>
        <source> Copyright (C) 2018 - %1 KangLin Studio</source>
        <translation>版权 (C) 2018 - %1 康林工作室</translation>
    </message>
</context>
<context>
    <name>CDlgOption</name>
    <message>
        <location filename="../../DlgOption.ui" line="14"/>
        <source>Option</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../../DlgOption.ui" line="28"/>
        <source>General</source>
        <translation>普通</translation>
    </message>
    <message>
        <location filename="../../DlgOption.ui" line="47"/>
        <source>Enable run from boot</source>
        <translation>允许开户自启动</translation>
    </message>
    <message>
        <location filename="../../DlgOption.ui" line="34"/>
        <source>Show windows in application start</source>
        <translation>应用启动时，显示窗口</translation>
    </message>
</context>
<context>
    <name>CFrmEyeNurse</name>
    <message>
        <location filename="../../FrmEyeNurse.ui" line="14"/>
        <source>Eye nurse</source>
        <translation>眼睛护士</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="vanished">显示</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Enable run from boot</source>
        <translation type="vanished">允许开户自启动</translation>
    </message>
    <message>
        <source>Disable run from boot</source>
        <translation type="vanished">禁止开机自启动</translation>
    </message>
    <message>
        <source>Protect eyesight</source>
        <translation type="vanished">保护视力</translation>
    </message>
    <message>
        <source>Work</source>
        <translation type="vanished">工作</translation>
    </message>
    <message>
        <source>Will want to lock the screen</source>
        <translation type="vanished">将要锁屏</translation>
    </message>
    <message>
        <source>Prompt to lock the screen and rest</source>
        <translation type="vanished">提示锁屏和休息</translation>
    </message>
</context>
<context>
    <name>CFrmUpdater</name>
    <message>
        <location filename="../../FrmUpdater.ui" line="14"/>
        <source>Updater</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="73"/>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="105"/>
        <source>New version:</source>
        <translation>新版本：</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="121"/>
        <source>New architecture:</source>
        <translation>新架构</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="141"/>
        <source>Current version：</source>
        <translation>当前版本</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="157"/>
        <source>Current architecture：</source>
        <translation>当前架构</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="171"/>
        <location filename="../../FrmUpdater.cpp" line="435"/>
        <source>There is laster version</source>
        <translation>这是最新版本</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="191"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="205"/>
        <source>Every time</source>
        <translation>每次</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="221"/>
        <source>Every date</source>
        <translation>每天</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="238"/>
        <source>Every week</source>
        <translation>每周</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="251"/>
        <source>Every Month</source>
        <translation>每月</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="284"/>
        <source>OK(&amp;O)</source>
        <translation>确定(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.ui" line="297"/>
        <source>Close(&amp;C)</source>
        <translation>关闭(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="136"/>
        <source>Being download xml file</source>
        <translation>正在下载XML文件</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="137"/>
        <source>Being download update file</source>
        <translation>正在下载更新文件</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="138"/>
        <source>Being install update</source>
        <translation>正在安装更新</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="168"/>
        <source>Current archecture: %1</source>
        <translation>当前架构: %1</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="176"/>
        <source>Current version: %1</source>
        <translation>当前版本: %1</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="299"/>
        <source>Download network error: </source>
        <translation>下载网络错误：</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="318"/>
        <source>Download fail:</source>
        <translation>下载失败：</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="389"/>
        <source>Parse file %1 fail. It isn&apos;t xml file</source>
        <translation>解析文件 %1 失败， 它不是 XML 文件</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="440"/>
        <source>New version: %1</source>
        <translation>新版本: %1</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="442"/>
        <source>New architecture: %1</source>
        <translation>新架构: %1</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="448"/>
        <location filename="../../FrmUpdater.cpp" line="462"/>
        <location filename="../../FrmUpdater.cpp" line="475"/>
        <source>System is different</source>
        <translation>系统不同</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="455"/>
        <location filename="../../FrmUpdater.cpp" line="468"/>
        <location filename="../../FrmUpdater.cpp" line="482"/>
        <source>Architecture is different</source>
        <translation>架构不同</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="488"/>
        <source>There is a new version, is it updated?</source>
        <translation>有新的版本，是否更新？</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="508"/>
        <source>Being install update ......</source>
        <translation>正在安装更新 ……</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="513"/>
        <location filename="../../FrmUpdater.cpp" line="522"/>
        <source>Download file fail</source>
        <translation>下载文件失败</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="529"/>
        <source>Md5sum is different. </source>
        <translation>Md5校验和不同。</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="530"/>
        <source>Download file md5sum: </source>
        <translation>下载文件的MD5校验和:</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="532"/>
        <source>md5sum in Update.xml:</source>
        <translation>XML文件中的MD5校验和:</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="558"/>
        <source>Execute install program error.%1</source>
        <translation>执行安装错误：%1</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="564"/>
        <source>The installer has started, Please close the application</source>
        <translation>开始安装，请先关闭本程序</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="495"/>
        <location filename="../../FrmUpdater.cpp" line="656"/>
        <source>Download ......</source>
        <translation>下载 ……</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="802"/>
        <source>Package version</source>
        <translation>包版本</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="807"/>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="812"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="818"/>
        <source>Operating system</source>
        <translation>操作系统</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="823"/>
        <source>Platform</source>
        <translation>平台</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="828"/>
        <source>Architecture</source>
        <translation>架构</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="833"/>
        <source>MD5 checksum</source>
        <translation>MD5校验和</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="837"/>
        <source>Package download url</source>
        <translation>包下载URL</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater.cpp" line="842"/>
        <source>Min update version</source>
        <translation>最小更新版本</translation>
    </message>
</context>
<context>
    <name>CMainWindow</name>
    <message>
        <location filename="../../MainWindow.ui" line="14"/>
        <source>Tasks</source>
        <translation>任务</translation>
    </message>
    <message>
        <location filename="../../MainWindow.ui" line="32"/>
        <source>Help(&amp;H)</source>
        <translation>帮助(&amp;H)</translation>
    </message>
    <message>
        <location filename="../../MainWindow.ui" line="39"/>
        <source>Tools(&amp;T)</source>
        <translation>工具(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../MainWindow.ui" line="57"/>
        <source>About(&amp;A)</source>
        <translation>关于(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../MainWindow.ui" line="60"/>
        <location filename="../../MainWindow.ui" line="63"/>
        <location filename="../../MainWindow.ui" line="66"/>
        <location filename="../../MainWindow.cpp" line="34"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../../MainWindow.ui" line="75"/>
        <source>Exit(&amp;E)</source>
        <translation>即出(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../MainWindow.ui" line="84"/>
        <source>Option(&amp;O)</source>
        <translation>选项(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../MainWindow.ui" line="87"/>
        <location filename="../../MainWindow.ui" line="90"/>
        <location filename="../../MainWindow.ui" line="93"/>
        <source>Option</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../../MainWindow.ui" line="102"/>
        <source>Sink(&amp;S)</source>
        <translation>换肤(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../MainWindow.ui" line="105"/>
        <location filename="../../MainWindow.ui" line="108"/>
        <location filename="../../MainWindow.ui" line="111"/>
        <location filename="../../MainWindow.ui" line="114"/>
        <source>Sink</source>
        <translation>换肤</translation>
    </message>
    <message>
        <location filename="../../MainWindow.ui" line="123"/>
        <source>Update(&amp;U)</source>
        <translation>更新(&amp;U)</translation>
    </message>
    <message>
        <location filename="../../MainWindow.cpp" line="24"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../../MainWindow.cpp" line="28"/>
        <location filename="../../MainWindow.cpp" line="98"/>
        <source>Hide</source>
        <translation>隐藏</translation>
    </message>
    <message>
        <location filename="../../MainWindow.cpp" line="30"/>
        <location filename="../../MainWindow.cpp" line="102"/>
        <source>Show</source>
        <translation>显示</translation>
    </message>
    <message>
        <location filename="../../MainWindow.cpp" line="38"/>
        <source>Enable run from boot</source>
        <translation>允许开户自启动</translation>
    </message>
    <message>
        <location filename="../../MainWindow.cpp" line="174"/>
        <source>Open sink</source>
        <translation>打开皮肤</translation>
    </message>
</context>
</TS>
