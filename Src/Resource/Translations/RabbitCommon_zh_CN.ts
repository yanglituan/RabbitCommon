<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>CDlgAbout</name>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="20"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="62"/>
        <source>Informatioin</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="128"/>
        <source>Home page: https://github.com/KangLin/Tasks.git</source>
        <translation>主页： https://github.com/KangLin/Tasks.git</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="87"/>
        <source>Version: 1.0.0.0</source>
        <translation>版本： 1.0.0.0</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="109"/>
        <source>Build Date:</source>
        <translation>编译日期：</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="223"/>
        <source>Donation</source>
        <translation>捐赠</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="247"/>
        <source>License</source>
        <translation>许可协议</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="261"/>
        <source>Change log</source>
        <translation>修改日志</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="275"/>
        <source>Thanks</source>
        <translation>感谢</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="472"/>
        <source> Copyright (C) 2018 KangLin Studio</source>
        <translation>版权(C) 2018 康林工作室</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="479"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="30"/>
        <source>Author:KangLin</source>
        <translation>作者： 康林</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.ui" line="414"/>
        <source>Tasks</source>
        <translation>任务</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="68"/>
        <source> Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="68"/>
        <source> Arch:</source>
        <translation> 架构：</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="69"/>
        <source>Build date:%1 %2</source>
        <translation>编译日期：%1 %2</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="75"/>
        <source> Copyright (C) 2018 - %1 %2</source>
        <translation>版权 (C) 2018 - %1 %2</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="50"/>
        <source>Author: KangLin
Email:kl222@126.com</source>
        <translation>作者： 康林
邮件:kl222@126.com</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="52"/>
        <source>KangLin Studio</source>
        <translation>康林工作室</translation>
    </message>
    <message>
        <location filename="../../DlgAbout/DlgAbout.cpp" line="73"/>
        <source>Home page:</source>
        <translation>主页：</translation>
    </message>
</context>
<context>
    <name>CFrmUpdater</name>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="14"/>
        <source>Updater</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="73"/>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="105"/>
        <source>New version:</source>
        <translation>新版本：</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="121"/>
        <source>New architecture:</source>
        <translation>新架构</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="141"/>
        <source>Current version：</source>
        <translation>当前版本</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="157"/>
        <source>Current architecture：</source>
        <translation>当前架构</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="171"/>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="495"/>
        <source>There is laster version</source>
        <translation>这是最新版本</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="191"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="205"/>
        <source>Every time</source>
        <translation>每次</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="221"/>
        <source>Every date</source>
        <translation>每天</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="238"/>
        <source>Every week</source>
        <translation>每周</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="251"/>
        <source>Every Month</source>
        <translation>每月</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="284"/>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="175"/>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="556"/>
        <source>OK(&amp;O)</source>
        <translation>确定(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.ui" line="297"/>
        <source>Close(&amp;C)</source>
        <translation>关闭(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="167"/>
        <source>Being download xml file</source>
        <translation>正在下载XML文件</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="181"/>
        <source>Being download update file</source>
        <translation>正在下载更新文件</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="185"/>
        <source>Being install update</source>
        <translation>正在安装更新</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="214"/>
        <source>Current archecture: %1</source>
        <translation>当前架构: %1</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="222"/>
        <source>Current version: %1</source>
        <translation>当前版本: %1</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="342"/>
        <source>: downloading %1%</source>
        <translation>: 正在下载 %1%</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="349"/>
        <source>Download network error: </source>
        <translation>下载网络错误：</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="368"/>
        <source>Download fail:</source>
        <translation>下载失败：</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="449"/>
        <source>Parse file %1 fail. It isn&apos;t xml file</source>
        <translation>解析文件 %1 失败， 它不是 XML 文件</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="500"/>
        <source>New version: %1</source>
        <translation>新版本: %1</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="502"/>
        <source>New architecture: %1</source>
        <translation>新架构: %1</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="508"/>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="522"/>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="535"/>
        <source>System is different</source>
        <translation>系统不同</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="515"/>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="528"/>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="542"/>
        <source>Architecture is different</source>
        <translation>架构不同</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="548"/>
        <source>There is a new version, is it updated?</source>
        <translation>有新的版本，是否更新？</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="565"/>
        <source>Hide</source>
        <translation>隐藏</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="577"/>
        <source>Being install update ......</source>
        <translation>正在安装更新 ……</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="582"/>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="591"/>
        <source>Download file fail</source>
        <translation>下载文件失败</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="598"/>
        <source>Md5sum is different. </source>
        <translation>Md5校验和不同。</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="599"/>
        <source>Download file md5sum: </source>
        <translation>下载文件的MD5校验和:</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="601"/>
        <source>md5sum in Update.xml:</source>
        <translation>XML文件中的MD5校验和:</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="627"/>
        <source>Execute install program error.%1</source>
        <translation>执行安装错误：%1</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="633"/>
        <source>The installer has started, Please close the application</source>
        <translation>开始安装，请先关闭本程序</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="740"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="566"/>
        <source>Download ......</source>
        <translation>下载 ……</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="741"/>
        <source>Is updating, be sure to close?</source>
        <translation>正在更新，是否关闭？</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="884"/>
        <source>Package version</source>
        <translation>包版本</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="889"/>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="894"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="900"/>
        <source>Operating system</source>
        <translation>操作系统</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="905"/>
        <source>Platform</source>
        <translation>平台</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="910"/>
        <source>Architecture</source>
        <translation>架构</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="915"/>
        <source>MD5 checksum</source>
        <translation>MD5校验和</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="919"/>
        <source>Package download url</source>
        <translation>包下载URL</translation>
    </message>
    <message>
        <location filename="../../FrmUpdater/FrmUpdater.cpp" line="924"/>
        <source>Min update version</source>
        <translation>最小更新版本</translation>
    </message>
</context>
</TS>
