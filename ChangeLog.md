- v0.0.4
  - Add screen power
  - Use QScrollArea in Android
  - Add calendar

- v0.0.3
  - Modify main window
  - TasksList and StickyList add toolbar
  - Library interface
  - Modify project directory
  - Add cmake manage project
  
- v0.0.2
  - Edit tasks
  - Updater
  - Sticky
  
- v0.0.1
  - Eye Nurse： Vision protection
